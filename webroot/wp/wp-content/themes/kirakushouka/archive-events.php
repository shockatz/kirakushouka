@layout('layout')
@include('utility')

@section('nav')@endsection
@section('snav')@endsection

@section('title')イベント情報一覧@endsection

@section('head')
<script type="text/javascript">
	$(function () {
		$('#content').addClass('events');
		$('ul>li.events').addClass('active');
	});
</script>
@endsection

@section('breadcrumb')
	<li>イベント情報一覧</li>
@endsection

@section('main')
<section id="events_archive">
	<h2><img src="/images/events/tit_ev_arc.png" height="37" width="234" alt="イベント情報一覧"></h2>
	<div class="archive">
	@wpposts
		<dl>
			<?php
				$excerpt	= (get_field('excerpt') ? get_field('excerpt') : '表題がありません');
				$terms		= get_the_terms(get_the_ID(), "event_type");
				if ($terms){
					$params = array();
					foreach($terms as $term){
						$param = array('className' => $term->slug,'classText' => $term->name);
						array_push($params, $param);
					}				
				}
			?>
			<dt>
				{{ DatetimeUtility::date( "JK年n月j日(x)", strtotime(get_the_date()) ) }}</dt>
			<dd>
				<?php foreach($params as $p){ ?>
					<span class="{{ $p['className'] }}">{{ $p['classText'] }}</span>
				<?php } ?>
				<a href="{{ the_permalink() }}">{{ $excerpt }}</a></dd>
		</dl>
	@wpempty
		<p>現在公開されているイベントがありません。</p>
	@wpend
	</div>

	<?php kriesi_pagination($additional_loop->max_num_pages); ?>

</section>


<nav>
	<h4>カテゴリー</h4>
	<ul>
		<a href="/wp/events"><li>すべてのカテゴリー</li></a>
		<a href="/wp/events/event_type/ftrip"><li>見学会</li></a>
		<a href="/wp/events/event_type/study"><li>勉強会</li></a>
		<a href="/wp/events/event_type/class_child"><li>子供向け教室</li></a>
		<a href="/wp/events/event_type/class_adult"><li>大人向け教室</li></a>
		<a href="/wp/events/event_type/other"><li>その他</li></a>
	</ul>
</nav>
@endsection
  