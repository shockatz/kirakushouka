@layout('layout')
@include('utility')

@section('nav')nav06@endsection
@section('snav')snav01@endsection

@section('title')
{{ the_title() }}@endsection

@section('head')
<script type="text/javascript">
	$(function () {
		$('#content').addClass('events');
		$('ul>li.events').addClass('active');
	});
</script>
@endsection

@section('breadcrumb')
	<li><a href="/wp/events">イベント情報一覧</a></li>
	<li>{{ the_title() }}</li>
@endsection

@section('main')
	<?php
		$excerpt	= (get_field('excerpt') ? get_field('excerpt') : '表題がありません');
		$terms		= get_the_terms(get_the_ID(), "event_type");
		if ($terms){
			$params = array();
			foreach($terms as $term){
				$param = array('className' => $term->slug,'classText' => $term->name);
				array_push($params, $param);
			}				
		}
	?>
	<section id="events_detail">
		<h2><img src="/images/events/tit_ev_det.gif" height="37" width="171" alt="イベント情報"></h2>

		@wpposts
		<div class="det_tit">
			<h3>{{ $excerpt }}</h3>
		</div>
		<dl>
			<dt>
				{{ DatetimeUtility::date( "JK年n月j日(x)", strtotime(get_the_date()) ) }}</dt>
			<dd>
				<?php foreach($params as $p){ ?>
					<span class="{{ $p['className'] }}">{{ $p['classText'] }}</span>
				<?php } ?>
				{{ the_content() }}
			</dd>
		</dl>
		@wpempty
			<p>現在公開されているイベントがありません。</p>
		@wpend
		<a class="to_arc" href="/wp/events"><p>イベント情報 一覧へ</p></a>
	</section>

	<nav>
		<h4>カテゴリー</h4>
		<ul>
		<a href="/wp/events"><li>すべてのカテゴリー</li></a>
		<a href="/wp/events/event_type/ftrip/"><li>見学会</li></a>
		<a href="/wp/events/event_type/study/"><li>勉強会</li></a>
		<a href="/wp/events/event_type/class_child/"><li>子供向け教室</li></a>
		<a href="/wp/events/event_type/class_adult/"><li>大人向け教室</li></a>
		<a href="/wp/events/event_type/other/"><li>その他</li></a>
		</ul>
	</nav>
@endsection



