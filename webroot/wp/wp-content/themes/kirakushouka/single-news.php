@layout('layout')
@include('utility')

@section('nav')@endsection
@section('snav')@endsection

@section('title')
{{ the_title() }}@endsection

@section('head')
<script type="text/javascript">
	$(function () {
		$('#content').addClass('events');
		// $('ul>li.events').addClass('active');
	});
</script>
@endsection

@section('breadcrumb')
	<li><a href="/wp/news">お知らせ一覧</a></li>
	<li>{{ the_title() }}</li>
@endsection

@section('main')
	<?php
		$excerpt	= (get_field('excerpt') ? get_field('excerpt') : '表題がありません');
		$terms		= get_the_terms(get_the_ID(), "news_type");
		if ($terms){
			$params = array();
			foreach($terms as $term){
				$param = array('className' => $term->slug,'classText' => $term->name);
				array_push($params, $param);
			}				
		}
	?>
		<section id="news_detail">
			<h2>NEWS</h2>
			@wpposts
			<div class="det_tit">
				<h3>{{ $excerpt }}</h3>
			</div>
			<dl>
				<dt>
					{{ the_time('Y.m.d') }}
					<?php // echo DatetimeUtility::date( "JK年n月j日(x)", strtotime(get_the_date())) ?></dt>
				<dd>
					<?php foreach($params as $p){ ?>
						<span class="{{ $p['className'] }}">{{ $p['classText'] }}</span>
					<?php } ?>
					{{ the_content() }}
				</dd>
			</dl>
			@wpempty
				<p>指定されたお知らせの内容が存在しません</p>
			@wpend
			<a class="to_arc" href="/index.html#news"><p>NEWS 新着情報へ</p></a>
	</section>
@endsection