@layout('layout')
@include('utility')

@section('nav')@endsection
@section('snav')@endsection

@section('title')
{{ the_title() }}@endsection

@section('head')
<style type="text/css">
	.viewBox {
		width : 340px;
		height : 255px;
		border:solid 1px silver;
	}
</style>
<script src="/js/imgLiquid-min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#content').addClass('examples');
		$('ul>li.examples').addClass('active');
		$('.viewBox').imgLiquid();
	});
</script>
@endsection

@section('breadcrumb')
	<li><a href="/wp/examples">設計・施工事例一覧</a></li>
	<li>{{ the_title() }}</li>
@endsection

@section('main')
	<section id="examples_detail">
		@wpposts
		<?php
			$comment	= (get_field('comment') ? get_field('comment') : '表題がありません');
			$terms		= get_the_terms(get_the_ID(), "house_type");
			if ($terms){
				$params = array();
				foreach($terms as $term){
					$param = array('className' => $term->slug,'classText' => $term->name);
					array_push($params, $param);
				}
			}
		?>
		<h2>
		<img src="/images/examples/tit_ex_det.png" height="37" width="191" alt="設計・施工事例"></h2>
		<div class="det_tit">
			<h3>{{ get_field('company') }}&ensp;{{ get_field('house_name') }}<span>[{{ get_field('city') }}]</span></h3>
		</div>
		<p>
		<?php foreach($params as $p){ ?>
			<span class="{{ $p['className'] }}">{{ $p['classText'] }}</span>
		<?php } ?>
		{{ $comment }}</p>
		@acfrepeater('content')
		<dl class="clearfix">
			<dt>{{ get_sub_field('title_sub') }}</dt>
			<dd>
				<div class="viewBox"><img src="{{ getAcfSubImageUrl('sub_photo1', 'medium') }}"></div>{{ get_sub_field('sub_comment1') }}</dd>
			<dd>
				<div class="viewBox"><img src="{{ getAcfSubImageUrl('sub_photo2', 'medium') }}"></div>{{ get_sub_field('sub_comment2') }}</dd>
		</dl>
		@acfend
		<a class="to_arc" href="/wp/examples"><p>設計・施工事例 一覧へ</p></a>
		@wpempty
		<p>設計・施工事例がありません。</p>
		@wpend
	</section>

	<!--設計・施工事例/カテゴリ -->
	<nav>
		<h4>カテゴリー</h4>
		<ul>
			<a href="/wp/examples"><li>すべてのカテゴリー</li></a>
			<a href="/wp/examples/house_type/f2"><li>2階建住宅</li></a>
			<a href="/wp/examples/house_type/f3"><li>3階建住宅</li></a>
			<a href="/wp/examples/house_type/f1"><li>平屋住宅</li></a>
			<a href="/wp/examples/house_type/dage"><li>2世帯住宅</li></a>
			<a href="/wp/examples/house_type/build"><li>店舗・ビル</li></a>
			<a href="/wp/examples/house_type/other"><li>その他</li></a>
		</ul>
	</nav>
@endsection


