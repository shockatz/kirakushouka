<!DOCTYPE html>
<html lang="ja"><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" --><head>

<meta charset="UTF-8">
<!-- InstanceParam name="classParam" type="text" value="wp" -->
<!-- InstanceParam name="pageParam" type="text" value="wp" -->
<!-- InstanceBeginEditable name="title" -->
<title>@yield('title')｜木楽匠家</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/style.css" media="all" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="/js/yuga.js"></script>
<!--[if lt IE 9]>
<script src="/js/html5shiv-printshiv.js"></script>
<script src="/js/rem.min.js"></script>
<![endif]-->
<!-- InstanceBeginEditable name="head" -->
@yield('head')
<?php wp_head(); ?>
<!-- InstanceEndEditable -->
</head>
<body id="top">

<!-- header -->
<header id="header">
	<h1><a href="/"><img src="/images/common/logo.png" height="191" width="687" alt="木楽匠家"></a></h1>
	<nav class="">
		<ul>
			<li class="index "><a href="/index.html">HOME</a></li>
			<li class="about "><a href="/about/index.html">木×楽×匠×家とは</a></li>
			<li class="examples "><a href="/wp/examples">設計・施工事例</a></li>
			<li class="events "><a href="/wp/events">イベント情報</a></li>
			<li class="member "><a href="/member/index.html">会員紹介</a></li>
			<li><img src="/images/common/nav_img.png" height="35" width="268" alt="お問い合わせ076-277-2252 担当:宮内"></li>
		</ul>
	</nav>
</header>
<!-- header -->

<!-- トップページ以外/パンくずリスト -->
<ol class="breadcrumb">
	<li><a href="/">ホーム</a></li>
	<!-- InstanceBeginEditable name="breadcrumb" -->
	<li>@yield('title')</li>
	<!-- InstanceEndEditable -->
</ol>


<!-- content -->
<div id="content" class="wp">
	<article class="clearfix">
<!-- InstanceBeginEditable name="EditRegion" -->
		@yield('main')
<!-- InstanceEndEditable -->
	</article>
	<p id="to_top"><a href="#top"><img src="/images/common/pagetop.png" height="48" width="48" alt="ページのトップへ"></a></p>
</div>
<!-- content -->
<!-- footer -->
<footer class="clearfix">
	<div>
		<address>
			●お問い合わせ先<br>
			事務局：あさひ木材（株）<br>
			〒924-0855 石川県白山市水島町879-1  TEL.076-277-2252 FAX.076-277-2370<br>
			E-mail:<a href="mailto:asahi.miyauchi@kirakusyoka.com">asahi.miyauchi@kirakusyoka.com</a>
		</address>
		<p>Copyright &copy; 2014 KI.RAKU.SYO.KA. All Rights Reserved.</p>
	</div>
</footer>
<!-- footer -->
<!-- InstanceBeginEditable name="foot" -->
<?php wp_footer(); ?>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>