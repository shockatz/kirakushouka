@layout('layout')
@include('utility')

@section('nav')@endsection
@section('snav')@endsection

@section('title')設計・施工事例一覧@endsection

@section('head')
<style type="text/css">
	.viewBox {
		width : 210px;
		height : 157px;
	}
</style>
<script src="/js/imgLiquid-min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#content').addClass('examples');
		// $('img.listview').wrap("<div class='viewBox'></div>");
		$('.viewBox').imgLiquid();
	});
</script>
@endsection

@section('breadcrumb')
	<li>設計・施工事例一覧</li>
@endsection

@section('main')
	<section id="examples_archive">
	<h2><img src="/images/examples/tit_ex_arc.png"/></h2>
	<div class="clearfix">
	@wpposts
		<?php
			$excerpt	= (get_field('comment') ? get_field('comment') : '表題がありません');
			$terms		= get_the_terms(get_the_ID(), "house_type");
			if ($terms){
				$params = array();
				foreach($terms as $term){
					$param = array('className' => $term->slug,'classText' => $term->name);
					array_push($params, $param);
				}				
			}
		?>
		<a href="{{ the_permalink() }}">
		<dl>
			<dt>{{ get_field('company') }}&ensp;{{ get_field('house_name') }}</dt>
			<dd>[{{ get_field('city') }}]</dd>
			<dd><div class="viewBox"><img class="listview" src="{{ getAcfImageUrl('photo', 'medium') }}" alt="{{ get_field('company') }}{{ get_field('city') }}{{ get_field('house_name') }}"></div></dd>
			<dd>
				<?php foreach($params as $p){ ?>
					<span class="{{ $p['className'] }}">{{ $p['classText'] }}</span>
				<?php } ?>
				{{ $excerpt }}
			</dd>
		</dl></a>
	@wpempty
		<p>設計・施工事例がありません。</p>
	@wpend
	</div>
</section>
<nav>
	<h4>カテゴリー</h4>
		<ul>
			<a href="/wp/examples"><li>すべてのカテゴリー</li></a>
			<a href="/wp/examples/house_type/f2"><li>2階建住宅</li></a>
			<a href="/wp/examples/house_type/f3"><li>3階建住宅</li></a>
			<a href="/wp/examples/house_type/f1"><li>平屋住宅</li></a>
			<a href="/wp/examples/house_type/dage"><li>2世帯住宅</li></a>
			<a href="/wp/examples/house_type/build"><li>店舗・ビル</li></a>
			<a href="/wp/examples/house_type/other"><li>その他</li></a>
		</ul>
</nav>
@endsection
  