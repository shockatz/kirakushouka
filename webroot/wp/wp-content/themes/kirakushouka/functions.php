<?php

//++++++++++++++++++++++++++++++++++++++++++++++
//アイキャッチ画像
// アイキャッチ画像の追加（投稿及び固定ページで有効にする）
add_theme_support('post-thumbnails');


//++++++++++++++++++++++++++++++++++++++++++++++
// 不要な wp_head() を除去する.(wp-includes/default-filters.php)
// ヘッダーメニューは管理画面のユーザーで消す。
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'rel_canonical');


//++++++++++++++++++++++++++++++++++++++++++++++
//Trust Form http://trust-form.org/auto-input-zip/
// 住所の自動入力
//add_action( 'wp_enqueue_scripts', 'my_ajaxzip3' ); function my_ajaxzip3() { wp_enqueue_script( 'ajaxzip3', 'http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js' ); } 


//++++++++++++++++++++++++++++++++++++++++++++++
// 管理画面以外へのjavascript読込禁止
//function stop_jQuery() {
//   if ( !is_admin() ) {
//      wp_deregister_script('jquery');
//   }
//}
//add_action('init', 'stop_jQuery');

//++++++++++++++++++++++++++++++++++++++++++++++
//CSS/jsファイルの読み込みを一括管理

/*
if (!is_admin()) {
	
	function register_style() {
		wp_register_style('reset', get_bloginfo('template_directory').'/css/reset.css');
	}
  function add_stylesheet() {
	  register_style();
	  wp_enqueue_style('reset');
	  if (is_home()) {
	    wp_enqueue_style('home');
	  } elseif (is_page(array('contct','subscription','app_withdrawal'))) {
	    wp_enqueue_style('contents');
	  }	elseif (is_page('member')) {
	    wp_enqueue_style('contents');
	  } elseif (is_page('profile')) {
	    wp_enqueue_style('contents');
	  }	    
	}
	add_action('wp_enqueue_scripts', 'add_stylesheet');

	function register_script(){
		wp_register_script('base', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js');
	}

	function add_script() {
		register_script();
		wp_enqueue_script('base');
    if (is_home()){
			wp_enqueue_script('knockout');
		}
		elseif (is_page('contct')) {
			wp_enqueue_script('commonjs');
	  }
		elseif (is_page('member')) {
			wp_enqueue_script('commonjs');
	  }	    		
		elseif (is_page('profile')) {
			wp_enqueue_script('commonjs');
	  }
		elseif (is_page('subscription')) {
			wp_enqueue_script('commonjs');
	  }
		elseif (is_page('app_withdrawal')) {
			wp_enqueue_script('commonjs');
	  }
	}
	add_action('wp_print_scripts', 'add_script');
}
*/

// バージョン表記を消す
function remove_cssjs_ver( $src ) {
	if( strpos( $src, '?ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );


// post_type & taxonomy
function add_website_post_types() {
	// post_type
	$params = array(
		 'labels' => array(
			 'add_new' => '新規追加',
			 'add_new_item' => '新規追加',
			 'edit_item' => '編集',
			 'new_item' => '新規',
			 'all_items' => '一覧',
			 'view_item' => '説明を見る',
			 'search_items' => '検索する',
			 'not_found' => '見つかりませんでした。',
			 'not_found_in_trash' => 'ゴミ箱内に見つかりませんでした。'
		 ),
		 'public' => true,
		 'has_archive' => true,
		 'supports' => array(
			 'title',
			 'editor'
		 )
	 );

	// regist post_type：news
	$params['labels']['name'] = 'NEWS';
	$params['labels']['singular_name'] = 'NEWS';
	register_post_type('news', $params);

	// regist post_type：examples
	$params['labels']['name'] = '設計・施工例';
	$params['labels']['singular_name'] = '設計・施工例';
	register_post_type('examples', $params);

	// regist post_type：examples
	$params['labels']['name'] = 'イベント';
	$params['labels']['singular_name'] = 'イベント';
	register_post_type('events', $params);

	// taxonomy
	$args = array(
		'public' => true,
		'show_ui' => true,
		'hierarchical' => true
	);
	$args['label'] = 'NEWSカテゴリ';	// regist_taxonomy : news_type
	register_taxonomy('news_type','news',$args);
	$args['label'] = '住宅タイプ';	// regist_taxonomy : house_type
	register_taxonomy('house_type','examples',$args);
	$args['label'] = 'イベント種別';	// regist_taxonomy : event_type
	register_taxonomy('event_type','events',$args);
}
add_action('init', 'add_website_post_types');


function do_post_thumbnail_feeds($content) {
  global $post;
  if (has_post_thumbnail($post->ID)) {
      $content = '<div>' . get_the_post_thumbnail($post->ID) . '</div>' . $content;
  }
  return $content;
}
add_filter('the_excerpt_rss', 'do_post_thumbnail_feeds');
add_filter('the_content_feed', 'do_post_thumbnail_feeds');


// 先日付投稿許可
function forced_publish_future_post( $data, $postarr ) {
	// 予約投稿場合に'publish'にする
	if ( $data['post_status'] == 'future') {
		$data['post_status'] = 'publish';
	}
	return $data;
}
add_filter( 'wp_insert_post_data', 'forced_publish_future_post', 10, 2 );


// Kuriesi Pager
function kriesi_pagination($pages = '', $range = 2) {  
	$showitems	= ($range * 2) + 1;  
	global			$paged;
	if (empty($paged)) {
		$paged		= 1;
	}
	if ($pages == ''){
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if (!$pages){
			$pages = 1;
		}
	}   
	
	if (1 != $pages) { // 1ページの時は非表示
		echo "<ul class='pager'>";
		if($paged > 2 && $paged > ($range + 1) && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo;</a></li>\n";
		if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>\n";
		for ($i=1; $i <= $pages; $i++) {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
				echo ($paged == $i)? "<li class='active'>".$i."</li>" : "<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
			}
		}
		if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a></li>"; 
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>&raquo;</a></li>";
		echo "</ul>\n";
	}
}

// pre_get_post
function change_posts_per_page($query) {
   if ( is_admin() || !$query->is_main_query() )
      return;
   // post_type : examples
   if ( $query->is_post_type_archive( 'examples' )) {
      $query->set( 'posts_per_page', 12 );
   }
   // taxonomy : house_type
   if ( $query->is_tax( 'house_type' )) {
      $query->set( 'posts_per_page', 12 );
   }
}
add_action( 'pre_get_posts', 'change_posts_per_page' );