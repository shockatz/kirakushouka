<?php

//== ACF関連関数

// ACF ID指定画像 field名・サイズ指定でHTMLタグ取得
function getAcfImageSrc($fieldName, $size){
	$imageID = get_field($fieldName);
	if ($imageID) {
		$src = wp_get_attachment_image_src($imageID, $size);
		return '<img src="' . $src[0] . '" />';
	} else {
		return '<img src="/common/images/dami.jpg" />';
	}
}

// ACF ID指定画像 field名・サイズ指定でパス取得
function getAcfImageUrl($fieldName, $size){
	$imageID = get_field($fieldName);
	if ($imageID) {
		$src = wp_get_attachment_image_src($imageID, $size);
		return $src[0];
	} else {
		return "/common/images/dami.jpg";
	}
}

// ACF SubField ID指定画像 field名・サイズ指定でパス取得
function getAcfSubImageUrl($fieldName, $size){
	$imageID = get_sub_field($fieldName);
	if ($imageID) {
		$src = wp_get_attachment_image_src($imageID, $size);
		return $src[0];
	} else {
		return "/common/images/dami.jpg";
	}
}

// ACF urlアップロードへのリンク取得
function getAcfAttachedFileLink($fieldName){
	$attach = get_field($fieldName);
	if ($attach) {
		return ($attach.'" target="_blank');
	} else {
		return ("#");
	}
}

// urlアップロード・指定URL・パーマリンクを勘案しインフォメーションへのリンクを生成
function getHyperLink($file, $url){

	$attach = get_field($file);	// 優先はUpload
	if ($attach) {
		return array('href'=>$attach, 'target'=>'_blank');
	} else {
		$file_URL		= get_field($url);
		if ($file_URL){			$site_url		= get_site_url();
			if (($pos = strpos($site_url, "/wp")) > 0){
				$site_url	= _mb_substr($site_url, 0, $pos -1 );
			}
			if (strpos($file_URL, $site_url) !== FALSE){
				return array('href'=>$file_URL, 'target'=>'_self');
			} else {
				return array('href'=>$file_URL, 'target'=>'_blank');
			}
		} else {
			return array('href'=>get_the_permalink(), 'target'=>'_self');
		}
	}
}

// 指定タクソノミー・タームに属する投稿データを取得する
// $terms : スラッグ名配列 ex:array('event','information')
function getNewsByTerms($post_type, $taxonomy, $terms){
	$param_query = array(
		'post_type'=> $post_type,
		'posts_per_page' => 10,
		'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field' =>		'slug',
					'terms' =>		$terms
				)
		)
	);

	$cat_posts = new WP_Query($param_query);

	$cat_results	= array();
	if ($cat_posts->have_posts()){
		while ($cat_posts->have_posts()){
			$cat_posts->the_post();
			$terms = get_the_terms(get_the_ID(), $taxonomy);
			if ($terms){
				$params = array();
				foreach($terms as $term){
					$param = array('className' => $term->slug,'classText' => $term->name);
					array_push($params, $param);
				}				
			}
			$anchor					= getHyperLink('upload_file','url');
			$date_past1m		= strtotime("-1 month");
			$date						= strtotime(get_the_date());
			if ($date > $date_past1m){
				$isnew = true;
			} else {
				$isnew = false;
			}
			
			array_push(
				$cat_results,
				array(
					'terms'				=> $params,
					'id'					=> get_the_ID(),
					'isnew'				=> $isnew,
					'date'				=> date('Y/m/d', $date),
					'href'				=> $anchor['href'],
					'target'			=> $anchor['target'],
					'title'				=> get_the_title(),
					'excerpt'			=> get_field('excerpt')
				)
			);
		}
	}
	wp_reset_postdata();

	return $cat_results;
}

/**
 * 日時用汎用クラス
 */
class DatetimeUtility {
    /** 元号用設定 */
  private static $gengoList = array(
    array(
          'name' => '平成',
          'name_short' => 'H',
          'timestamp' => 600188400, //1989-01-08,
          ),
    array(
          'name' => '昭和',
          'name_short' => 'S',
          'timestamp' => -1357635600, //1926-12-25'
          ),
    array(
          'name' => '大正',
          'name_short' => 'T',
          'timestamp' => -1812186000, //1912-07-30
          ),
    array(
          'name' => '明治',
          'name_short' => 'M',
          'timestamp' => -3187242000, //1869-01-01 // 正確にはよくわからない
          ),
    );
    /** 日本語曜日設定 */
    private static $weekJp = array(
      0 => '日',
      1 => '月',
      2 => '火',
      3 => '水',
      4 => '木',
      5 => '金',
      6 => '土',
    );

    /**
     * 和暦などを追加したdate関数
     *
     * 追加した記号
     * J : 元号
     * b : 元号略称
     * K : 和暦年(1年を元年と表記)
     * k : 和暦年
     * x : 日本語曜日(0:日-6:土)
     */
    public static function date($format, $timestamp = null) {
      // 和暦関連のオプションがある場合は和暦取得
      $gengo = array();
      $timestamp = is_null($timestamp) ? time() : $timestamp;
      if (preg_match('/[J|b|K|k]/', $format)) {
          foreach (self::$gengoList as $g) {
              if ($g['timestamp'] <= $timestamp) {
                  $gengo = $g;
                  break;
              }
          }
      }
      // J : 元号
      if (preg_match('/J/', $format)) {
          $format = preg_replace('/J/', $gengo['name'], $format);
      }
      // b : 元号略称
      if (preg_match('/b/', $format)) {
          $format = preg_replace('/b/', $gengo['name_short'], $format);
      }
      // K : 和暦用年(元年表示)
      if (preg_match('/k/', $format)) {
          $year = date('Y', $timestamp) - date('Y', $gengo['timestamp']) + 1;
          $year = $year == 1 ? '元' : $year;
          $format = preg_replace('/k/', $year, $format);
      }
      // k : 和暦用年
      if (preg_match('/K/', $format)) {
          $year = date('Y', $timestamp) - date('Y', $gengo['timestamp']) + 1;
          $format = preg_replace('/K/', $year, $format);
      }
      // x : 日本語曜日
      if (preg_match('/x/', $format)) {
          $w = date('w', $timestamp);
          $format = preg_replace('/x/', self::$weekJp[$w], $format);
      }
      return date($format, $timestamp);
    }
}
