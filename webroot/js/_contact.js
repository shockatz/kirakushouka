﻿// javascript routine for JILLA entry.html
// coding by M.Hayashi

var app = $.sammy('body', function () {

	// URL/#/ 動作定義
	this.get('#/', function () {
		$('.entry').show();
		$('.confirm').hide();
		$('.thanks').hide();
	});
	// URL/#/entry 入力画面表示
	this.get('#/entry', function () {
		$('.entry').show();
		$('.confirm').hide();
		$('.thanks').hide();
	});
	// URL/#/confirm 確認画面表示
	this.get('#/confirm', function () {
		$('.entry').hide();
		$('.confirm').show();
		$('.thanks').hide();
	});
	// URL/#/thanks サンクス画面表示
	this.get('#/thanks', function () {
		$('.entry').hide();
		$('.confirm').hide();
		$('.thanks').show();
	});
});

function viewModel() {

	//this.addr		= ko.computedObservable(function () {
	//	return this.pref() + "-" + this.address();
	//}, this);
	//this.post		= ko.computedObservable(function () {
	//	return this.post1() + "-" + this.post2();
	//}, this);

	this.refCD				= ko.observable("");
	this.nickname			= ko.observable("");
	this.name				= ko.observable("");
	this.company			= ko.observable("");
	this.mail				= ko.observable("");
	this.tel					= ko.observable("");
	this.inqbody			= ko.observable("");
	this.errorExist		= ko.observable(false);
	this.isMemberAssigned = ko.observable(false);
}


$(function () {

	app.run('#/');

	var vm = new viewModel();
	ko.applyBindings(vm);

	var refCD;
	if ((refCD = getUrlVars()["refCD"]) != null){
		vm.refCD(refCD);
		$('#loading').show();
		$.ajax({
			url: host + "/API/getMemberNickname",
			type: "POST",
			data: { refCD: refCD },
			dataType: "json",
			// contentType: 'application/json',
			success: function (data) {
				$('#loading').hide();
				if (data.result){
					vm.nickname(data.nickname);
					vm.isMemberAssigned(true);
				} else {
					alert(data.message);
				}
			},
			error: function () {
				$('#loading').hide();
				alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
			}
		});
	};

	$('button').hover(
		function () {
			$(this).css('cursor', 'pointer');
		},
		function () {
			$(this).css('cursor', 'default');
		}
	);

	// 確認画面へ
	$('#goConfirm').click(function (e) {
		$("#entryForm").validate({
			rules: {
				// company: { required: true },
				name: { required: true },
				mail: { required: true, email: true },
				tel: { required: true, telnum: true },
				inqbody: { required: true }
			}

		});
		if (!$("#entryForm").valid()) {
			// alert("not valid");
			vm.errorExist(true);
			return false;
		}
		vm.errorExist(false);
		e.preventDefault();
		location.hash = "/confirm";
	});

	// 確認画面から戻る
	$('#goEntry').click(function (e) {
		e.preventDefault();
		location.hash = "/entry";
	});

	// 送信
	$('#goSend').click(function (e) {
		if (!confirm("上記の内容で送信します。\nよろしいですか？")) {
			return false;
		} else {
			$('#loading').show();
			e.preventDefault();
			var jsData = ko.toJS(vm);
			$.ajax({
				url: host + "/API/registerInquiry",
				type: "POST",
				data: jsData,
				dataType: "json",
				// contentType: 'application/json',
				success: function (res) {
					$('#loading').hide();
					if (res.result) {
						location.hash = "/thanks";
					}
				},
				error: function () {
					$('#loading').hide();
					alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
				}
			});

			//$('#entryForm').attr('action','mail_thanks.php');
			//$('#entryForm').attr('method', 'post');
			//$('#entryForm').submit();
		}
	});


});

