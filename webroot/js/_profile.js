﻿// javascript routine for JILLA entry.html
// coding by M.Hayashi

$(function () {

	$('#loading').hide();

	$('button').hover(
		function () {
			$(this).css('cursor', 'pointer');
		},
		function () {
			$(this).css('cursor', 'default');
		}
	);

	var refCD;
	if ((refCD = getUrlVars()["refCD"]) != null) {
		$('#loading').show();
		$.ajax({
			url: host + "/API/getMemberProfile",
			type: "POST",
			data: { refCD: refCD },
			dataType: "json",
			// contentType: 'application/json',
			success: function (data) {
				$('#loading').hide();
				if (data.result) {
					defineVM(data.profile);
				} else {
					alert(data.message);
				}
			},
			error: function () {
				$('#loading').hide();
				alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
			}
		});
	};

	$(".fancybox").fancybox();

});

function defineVM(data) {
	var vm = ko.mapping.fromJS(data);

	vm.nameBind = ko.computed(function () {
		return vm.nickname() + "（" + vm.nicknameYomi() + "）";
	});
	vm.inqurl = ko.computed(function () {
		return "/other/contact.html?refCD=" + vm.refCD();
	});
	vm.isNamePublic = ko.computed(function () {
		return vm.name() != "" ? true : false;
	});
	vm.isAddrPublic = ko.computed(function () {
		return vm.addr() != "" ? true : false;
	});
	vm.isTelPublic = ko.computed(function () {
		return vm.tel() != "" ? true : false;
	});
	vm.isKtaiPublic = ko.computed({
		read: function(){
			return vm.ktai() != "" ? true : false;
		},
		write: function (value) {
		}
	});

	vm.profile_img.thumb_url_abs = ko.computed(function () {
		return host + vm.profile_img.thumb_url();
	});
	if (!vm.isTelPublic()) vm.isKtaiPublic(false);

	ko.applyBindings(vm);
}

//name
//nickname
//nicknameYomi
//pref
//addr
//tel
//ktai
//url
//artwork
//comment
//profile
//price
//profile_img	{ isExist thumb_url ref_url }
//artfile_imgs[{ isExist thumb_url ref_url }]

// Knockoutパラメタを受け、内容非空白でtrue
function isStringNotEmpty(param) {
	if (param() == '' || param() == null) {
		return false;
	} else {
		return true;
	}
}

function getAbsPath(param) {
	return host + param();
}

function isArrayExist(param) {
	return (param().length > 0);
}