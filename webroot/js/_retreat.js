﻿// javascript routine for JILLA entry.html
// coding by M.Hayashi

var vm;

var app = $.sammy('body', function () {

	// URL/#/ 動作定義
	this.get('#/', function () {
		$('.entry').show();
		$('.confirm').hide();
		$('.thanks').hide();
	});
	// URL/#/entry 入力画面表示
	this.get('#/entry', function () {
		$('.entry').show();
		$('.confirm').hide();
		$('.thanks').hide();
	});
	// URL/#/confirm 確認画面表示
	this.get('#/confirm', function () {
		$('.entry').hide();
		$('.confirm').show();
		$('.thanks').hide();
	});
	// URL/#/thanks サンクス画面表示
	this.get('#/thanks', function () {
		$('#entryForm').hide();
		$('.entry').hide();
		$('.confirm').hide();
		$('.thanks').show();
	});
});

function viewModel() {

	//this.addr		= ko.computedObservable(function () {
	//	return this.pref() + "-" + this.address();
	//}, this);
	//this.post		= ko.computedObservable(function () {
	//	return this.post1() + "-" + this.post2();
	//}, this);

	this.ID			= ko.observable(0);
	this.pass		= ko.observable("");
	this.nameSei	= ko.observable("");
	this.nameMei	= ko.observable("");
	this.nickname	= ko.observable("");
	this.mail		= ko.observable("");
	this.inqDetail = ko.observable("");
	this.errorExist = ko.observable(false);

}

// ViewMethod Jobs/jobs_otherを連結して返す
function  getJobs(){

}

$(function () {

	app.run('#/');
	$('#loading').hide();
	$('button').hover(
		function(){
			$(this).css('cursor', 'pointer');
		},
		function (){
			$(this).css('cursor', 'default');
		}
	);

	// 文字種検証追加
	$.validator.addMethod(
	  "charAnk",
		function (value, element) {
			reg = new RegExp("^[0-9a-zA-Z\-_]+$");
			return this.optional(element) || reg.test(value);
		},
		"半角英数字(a-z, A-Z, 0-9)と記号「-_」が使用でき案す"
	 );

	// 確認画面へ
	$('#goConfirm').click(function (e) {
		$("#entryForm").validate({
			rules: {
				ID:{
					required: true,
					rangelength: [1, 8],
					digits: true
				},
				nameSei:		{ required: true },
				nameMei:		{ required: true },
				nickname:	{ required: true },
				tel1:			{ required: true },
				tel2:			{ required: true },
				tel3:			{ required: true },
				mail:			{ required: true, email: true },
				mailConf:	{ required: true, equalTo: "#mail" },
				pass: {
					required: true,
					rangelength: [4, 16],
					charAnk: true
				}
			},
			messages: {
				ID: {
					required: "必須",
					rangeLength: "1-8ケタの数値",
					digits: "数字のみ"
				},
				nameSei: { required: "必須" },
				nameMei: { required: "必須" },
				nickname: { required: "必須" },
				tel1: { required: "必須" },
				tel2: { required: "必須" },
				tel3: { required: "必須" },
				mail: { required: "必須", email: " 入力形式不正です" },
				mailConf: { required: "必須", equalTo: " 内容が異なっています" },
				pass: {
					required: "必須",
					rangeLength: "4から16文字で入力"
				}
			},
			groups: {
				tel: "tel1 tel2 tel3",
				ktaiTel: "ktaiTel1 ktaiTel2 ktaiTel3",
			},
			errorPlacement: function (error, element) {
				if (element.attr("name") == "sex") {
					error.insertAfter("#sexError");
				} else if (element.attr("name") == "insulance") {
					error.insertAfter("#insulanceError");
				} else {
					error.insertAfter(element);
				}
			}

		});
		if (!$("#entryForm").valid()) {
			// alert("not valid");
			vm.errorExist(true);
			return false;
		}
		vm.errorExist(false);
		e.preventDefault();
		location.hash = "/confirm";
	});

	// 確認画面から戻る
	$('#goEntry').click(function (e) {
		e.preventDefault();
		location.hash = "/entry";
	});

	// 送信
	$('#goSend').click(function (e) {
		if (!confirm("上記の内容で送信します。\nよろしいですか？")) {
			return false;
		} else {
			$('#loading').show();
			e.preventDefault();
			var jsData = ko.toJS(vm);
			$.ajax({
				url: host + "/API/registerEntry",
				type: "POST",
				data: jsData,
				dataType: "json",
				// contentType: 'application/json',
				success: function (res) {
					$('#loading').hide();
					if (res.result) {
						location.hash = "/thanks";
					} else {
						alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
					}
				},
				error: function () {
					$('#loading').hide();
					alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
				}
			});

			//$('#entryForm').attr('action','mail_thanks.php');
			//$('#entryForm').attr('method', 'post');
			//$('#entryForm').submit();
		}
	});

	vm = new viewModel();
	ko.applyBindings(vm);
	// alert("finished !!");

});

