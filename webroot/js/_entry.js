﻿// javascript routine for JILLA entry.html
// coding by M.Hayashi

var vm;

var app = $.sammy('body', function () {

	// URL/#/ 動作定義
	this.get('#/', function () {
		$('.entry').show();
		$('.confirm').hide();
		$('.thanks').hide();
	});
	// URL/#/entry 入力画面表示
	this.get('#/entry', function () {
		$('.entry').show();
		$('.confirm').hide();
		$('.thanks').hide();
	});
	// URL/#/confirm 確認画面表示
	this.get('#/confirm', function () {
		$('.entry').hide();
		$('.confirm').show();
		$('.thanks').hide();
	});
	// URL/#/thanks サンクス画面表示
	this.get('#/thanks', function () {
		$('#entryForm').hide();
		$('.entry').hide();
		$('.confirm').hide();
		$('.thanks').show();
	});
});




function zip2Addr(event) {
	if (($('input#addrPost1').val().length == 3) && ($('input#addrPost2').val().length == 4)) {
		window.AjaxZip3.zip2addr("addrPost1", "addrPost2", "addrPref", "addrCity", "dummy", "addrStreet");
		var timer = setInterval(function () {
			vm.addrPref($('select#addrPref').val());
			vm.addrCity($('input#addrCity').val());
			vm.addrStreet($('input#addrStreet').val());
		}, 3000);
	}
}

function viewModel() {

	//this.addr		= ko.computedObservable(function () {
	//	return this.pref() + "-" + this.address();
	//}, this);
	//this.post		= ko.computedObservable(function () {
	//	return this.post1() + "-" + this.post2();
	//}, this);

	this.nameSei = ko.observable("");
	this.nameMei = ko.observable("");
	this.yomiSei = ko.observable("");
	this.yomiMei = ko.observable("");
	this.nickname = ko.observable("");
	this.sex = ko.observable("");
	this.mail = ko.observable("");
	this.mailConf = ko.observable("");
	this.tel1 = ko.observable("");
	this.tel2 = ko.observable("");
	this.tel3 = ko.observable("");
	this.ktaiTel1 = ko.observable("");
	this.ktaiTel2 = ko.observable("");
	this.ktaiTel3 = ko.observable("");
	this.addrPost1 = ko.observable("");
	this.addrPost2 = ko.observable("");
	this.addrPref = ko.observable("");
	this.addrCity = ko.observable("");
	this.addrStreet = ko.observable("");
	this.addrBuild = ko.observable("");
	this.acc = ko.observable("");
	this.pass = ko.observable("");
	this.inqDetail = ko.observable("");
	this.insulance = ko.observable("1");
	this.jobs = ko.observableArray([]);
	this.jobs_other = ko.observable("");

	this.errorExist = ko.observable(false);

	this.sexTerm = ko.computed(function () {
		var answer = "未入力";
		switch (this.sex()) {
			case "1": answer = "男性"; break;
			case "2": answer = "女性"; break;
		}
		return answer;
	}, this);
	this.insulanceTerm = ko.computed(function () {
		var answer = "未入力";
		switch (this.insulance()) {
			case "1": answer = "希望する"; break;
			case "0": answer = "希望しない"; break;
		}
		return answer;
	}, this);
	this.joined_jobs = ko.computed(function () {
		var answer	= "";
		answer		= this.jobs().join('・');
		if (this.jobs_other()) answer += "・" + this.jobs_other();
		return answer;
	}, this)

}

// ViewMethod Jobs/jobs_otherを連結して返す
function  getJobs(){

}


$(function () {

	app.run('#/');

	$('#loading').hide();

	$('button').hover(
		function(){
			$(this).css('cursor', 'pointer');
		},
		function (){
			$(this).css('cursor', 'default');
		}
	);

	// 郵便番号入力
	$('input#addrPost1').keyup(zip2Addr);
	$('input#addrPost2').keyup(zip2Addr);

	//$('input#addrPost2').keyup(function () {
	//	if ($(this).val().length < 4) return;
	//	alert('fetched !!');
	//	window.AjaxZip3.zip2addr("addrPost1", "addrPost2", "addrPref", "addrCity", "dummy", "addrStreet");
	//	var timer = setInterval(function () {
	//		vm.addrPref($('select#addrPref').val());
	//		vm.addrCity($('input#addrCity').val());
	//		vm.addrStreet($('input#addrStreet').val());
	//	}, 3000);
	//});

	// 文字種検証追加
	$.validator.addMethod(
	  "charAnk",
		function (value, element) {
			reg = new RegExp("^[0-9a-zA-Z\-_]+$");
			return this.optional(element) || reg.test(value);
		},
		"半角英数字(a-z, A-Z, 0-9)と記号「-_」が使用でき案す"
	 );

	// 確認画面へ
	$('#goConfirm').click(function (e) {
		$("#entryForm").validate({
			rules: {
				nameSei:		{ required: true },
				nameMei:		{ required: true },
				yomiSei:		{ required: true },
				yomiMei:		{ required: true },
				nickname:	{ required: true },
				sex:			{ required: true },
				tel1:			{ required: true },
				tel2:			{ required: true },
				tel3:			{ required: true },
				ktaiTel1:	{ required: true },
				ktaiTel2:	{ required: true },
				ktaiTel3:	{ required: true },
				mail:			{ required: true, email: true },
				mailConf:	{ required: true, equalTo: "#mail" },
				addrPost1:		{ required: true },
				addrPost2:		{ required: true },
				addrPref:		{ required: true },
				addrCity:		{ required: true },
				addrStreet:		{ required: true },
				pass: {
					required: true,
					rangelength: [4, 16],
					charAnk: true
				},
				acc: {
					required: true,
					rangelength: [4, 16],
					charAnk: true,
					remote: {
						url: host + "/API/checkMemAcc",
						type: "post",
						dataType: "json",
					}
				}
			},
			messages: {
				nameSei: { required: "必須" },
				nameMei: { required: "必須" },
				yomiSei: { required: "必須" },
				yomiMei: { required: "必須" },
				nickname: { required: "必須" },
				sex: { required: "必須" },
				tel1: { required: "必須" },
				tel2: { required: "必須" },
				tel3: { required: "必須" },
				ktaiTel1: { required: "必須" },
				ktaiTel2: { required: "必須" },
				ktaiTel3: { required: "必須" },
				mail: { required: "必須", email: " 入力形式不正です" },
				mailConf: { required: "必須", equalTo: " 内容が異なっています" },
				addrPost1: { required: "必須" },
				addrPost2: { required: "必須" },
				addrPref: { required: "必須" },
				addrCity: { required: "必須" },
				addrStreet: { required: "必須" },
				acc: {
					required: "必須",
					rangeLength:"4から16文字で入力",
					remote: "他の会員が使用済みです"
				},
				pass: {
					required: "必須",
					rangeLength: "4から16文字で入力"
				}
			},
			groups: {
				tel: "tel1 tel2 tel3",
				ktaiTel: "ktaiTel1 ktaiTel2 ktaiTel3",
			},
			errorPlacement: function (error, element) {
				if (element.attr("name") == "sex") {
					error.insertAfter("#sexError");
				} else if (element.attr("name") == "insulance") {
					error.insertAfter("#insulanceError");
				} else {
					error.insertAfter(element);
				}
			}

		});
		if (!$("#entryForm").valid()) {
			// alert("not valid");
			vm.errorExist(true);
			return false;
		}
		vm.errorExist(false);
		e.preventDefault();
		location.hash = "/confirm";
	});

	// 確認画面から戻る
	$('#goEntry').click(function (e) {
		e.preventDefault();
		location.hash = "/entry";
	});

	// 送信
	$('#goSend').click(function (e) {
		if (!confirm("上記の内容で送信します。\nよろしいですか？")) {
			return false;
		} else {
			$('#loading').show();
			e.preventDefault();
			var jsData = ko.toJS(vm);
			$.ajax({
				url: host + "/API/registerEntry",
				type: "POST",
				data: jsData,
				dataType: "json",
				// contentType: 'application/json',
				success: function (res) {
					$('#loading').hide();
					if (res.result) {
						location.hash = "/thanks";
					} else {
						alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
					}
				},
				error: function () {
					$('#loading').hide();
					alert("送信エラーです。改めて送信してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
				}
			});

			//$('#entryForm').attr('action','mail_thanks.php');
			//$('#entryForm').attr('method', 'post');
			//$('#entryForm').submit();
		}
	});

	vm = new viewModel();
	ko.applyBindings(vm);
	// alert("finished !!");

});

