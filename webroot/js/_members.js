﻿// javascript routine for Galax contach.html
// coding by M.Hayahsi

// startup routine

var page		= 1;
var initVM	= true;
var vm = null;
var vm_qty = new ViewModel_PagerQty();

function ViewModel_PagerQty() {
	this.total			= ko.observable(0);
	this.pageStart		= ko.observable(1); // 1Basis
	this.pageLength	= ko.observable(0);
	this.start = ko.computed(function () {
		return (this.pageStart()-1) * 20 + 1;
	}, this);
	this.end	= ko.computed(function () {
		return (this.pageStart() - 1) * 20 + this.pageLength();
	}, this);
}

$(function() {

	ko.applyBindings(vm_qty, document.getElementById("pgIndicator"));

	$('#btnSearch').click(function (e) {
		page = 1;
		vm_qty.pageStart(1);

		$.ajax({	// 件数取得
			url: host + "/API/getMembers_count",
			type: "POST",
			dataType: "json",
			data: {
				sex: $('#sex').val(),
				pref: $('#pref').val(),
				name: $('#name').val(),
				page: window.page
			},
			success: function (data) {
				vm_qty.total(data.qty);
				$('div.pager').pagination({
					items: data.qty,
					itemsOnPage: 20,
					cssStyle: 'compact-theme',
					onPageClick: function (pg, event) {
						window.page = pg;
						getMember(window.page);
						//	// event.preventDefault();
						// $('#btnSearch').trigger('click');
					}
				});
				getMember(1);

			},
			error: function () {
				alert("Ajaxエラーです");
			}
		});

		//$('#entryForm').attr('action','mail_thanks.php');
		//$('#entryForm').attr('method', 'post');
		//$('#entryForm').submit();
	});

	$('#btnSearch').trigger('click');
});

function getMember(page) {
	vm_qty.pageStart(page);
	$.ajax({
		url: host + "/API/getMembers_wPaging",
		type: "POST",
		dataType: "json",
		data: {
			sex: $('#sex').val(),
			pref: $('#pref').val(),
			name: $('#name').val(),
			skips: (page - 1) * 20,
			rows: 20
		},
		success: function (data) {
			vm_qty.pageLength(data.infos.length);
			defineVM(data);
		},
		error: function () {
			alert("Ajaxエラーです");
		}
	});
}

function defineVM(data) {
	// refCD
	// profimg
	// ID
	// nickname
	if (initVM) {
		// 初期表示 Table部vm定義と反映
		window.vm = ko.mapping.fromJS(data);
		initVM = false;
		ko.applyBindings(window.vm, document.getElementById("membersTable"));
	} else {
		// 取得結果をViewModelに反映
		window.vm.infos.removeAll();
		$.each(data.infos, function (index, info) {
			window.vm.infos.push(ko.mapping.fromJS(info));
		});
		// window.vm = ko.mapping.fromJS(data);
		// ko.applyBindings(window.vm);
		// window.vm.infos = data.infos;
	}
}

function ControlViewModel() {
	this.page	= ko.observable(1);
	this.sex		= ko.observable("0");
	this.pref	= ko.observable("");
	this.name = ko.observable("");

}



function profImgPath(ID, profimg) {
	if (profimg() !== null) {
		return host + "/uploaded/artfiles/" + ID() + "/Profile_58/" + profimg();
	} else {
		return host + "/uploaded/artfiles/profile_58.png";
	}
}

function profileHref(refCD) {
	return "profile.html?refCD=" + refCD();
}

function contactHref(refCD) {
	return "/other/contact.html?refCD=" + refCD();
}