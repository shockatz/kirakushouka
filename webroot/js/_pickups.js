﻿// javascript routine
// coding by M.Hayahsi
$(function () {
	$('#pickups').hide();
	$.ajax({
		url: host + "/API/getPickups",
		type: "POST",
		dataType: "json",
		// contentType: 'application/json',
		success: function (data) {
			$('#pickups').show();
			if (data.result) {
				defineVM(data);
			} else {
				alert(data.message);
			}
		},
		error: function () {
			alert("Ajax通信エラーです。ページを再読込してみてください。\nこのエラーが何度も出る場合は、ブラウザを最新のものに変更して再度、送信してみてください。");
		}
	});
});

function defineVM(data) {
	// refCD
	// profimg
	// ID
	// nickname
	var vm = ko.mapping.fromJS(data);
	$.each(vm.pickups(), function (index, pickup) {

		pickup.profileHref = ko.computed(function () {
			return "/about/profile.html?refCD=" + pickup.refCD();
		});
		pickup.profImgPath = ko.computed(function () {
			if (pickup.profimg() == null) {
				return (host + "/uploaded/artfiles/profile_58.png");
			} else {
				return host + "/uploaded/artfiles/" + pickup.ID() + "/Profile_58/" + pickup.profimg();
			}
		});
	});
	ko.applyBindings(vm);

	$('#pickups-members').carouFredSel({
		visible: 5,
		direction: "left",
		scroll: {
			items: 5,
			// easing: "elastic",
			duration: 2000,
			pauseOnHover: true
		}
	});

}