﻿// var host = "http://localhost:52732";
var host = "http://server.jilla.or.jp";

$(function () {

	// hide second-nav panel
	$('#g-nav ul.second-nav').hide();
	$('#s-nav>ul').hide();

	var nav = $('body').attr('data-nav');
	var snav = $('body').attr('data-snav');

	// markup assigned menu as 'active'
	if (nav) {
		$("#g-nav>ul>li." + nav).addClass('act');
		$("#s-nav>ul." + nav).show();
	}

	// implement navigation pulldown for menues 'not active'
	$('#g-nav>ul>li:not([class~=act])').on({
		 'mouseenter': function(){
			$(this).addClass('act');
			$(this).find('.second-nav').show();
		 },
		 'mouseleave': function(){
			$(this).removeClass('act');
			$(this).find('.second-nav').hide();
		 }
	});

//	$("#nav").on('mouseenter','li:not([class~=act])',function(){
//		$(this).addClass('act');
//		$(this).find('.second-nav').show();
//	});

	// maintain header scroll position
	var nav = $('#nav');
	var offset = nav.offset();
	$(window).scroll(function () {
		if ($(window).scrollTop() > offset.top) {
			nav.addClass('fixed');
		} else {
			nav.removeClass('fixed');
		}
	});
});


// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
	var vars = [], hash;
	var hashes = window.location.search.slice(window.location.search.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}



/*
    //Class「key」を付ける
    $('li').click(function(evt){
        console.log('click');
	var $this = $(this);
	
	if (!$this.hasClass('key')) {
            $this.addClass('key');
	    evt.stopPropagation();
	}
    });
    //Class「key」をクリックで背景色を赤色に変更
    $(document).delegate('.key', 'click', function(){
        console.log('delegate');
        $(this).css('backgroundColor', 'red');
    });
*/
